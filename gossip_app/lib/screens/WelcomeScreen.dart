import 'package:catchforms/constants/ui/gradients.dart';
import 'package:catchforms/models/userModel.dart';
import 'package:catchforms/routes/router.dart';
import 'package:catchforms/store/store.dart';
import 'package:catchforms/utils/firebase/FirebaseAuthentications.dart';
import 'package:catchforms/utils/firebase/FirebaseCloudMessaging.dart';
import 'package:catchforms/utils/Logger.dart';
import 'package:catchforms/utils/QueryType.dart';
import 'package:catchforms/utils/ui/SetScreenDimension.dart';
import 'package:catchforms/utils/ui/Snackbar.dart';
import 'package:catchforms/utils/ui/clippers/ClipType.dart';
import 'package:catchforms/utils/ui/clippers/ContainerClipper.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:catchforms/utils/ui/dialogs/ProgressDialog.dart';
import 'package:flutter/material.dart';

class WelcomeScreen extends StatefulWidget {
  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  final WelcomeScreenScaffoldKey = GlobalKey<ScaffoldState>();

  bool _showLogin = false;

  @override
  void initState() {
    super.initState();

    silentLogin().then((user) {
      if (user != null) {
        _userProfile(user);
      } else {
        // _showLogin = true;
        showLoaderDialog(context: context, message: "Please wait.");
        gmailSignIn().then((user){
          Log.d(tag: "User", message: user.toString());
           _userProfile(user);
        }).catchError((error){
        Log.d(tag: "Authentication Error", message: error.toString());
        Navigator.pop(context);

        // messageDialog(context: context);


           Alert(
      context: context,
      type: AlertType.info,
      title: "Login",
      desc: "Please login with gmail to continue.",
      buttons: [
        DialogButton(
          child: Text(
            "Close",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: ((){
            Navigator.pop(context);
            setState(() {
          _showLogin = true;
        });
          }),
          width: 120,
        )
      ],
    ).show();

        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    SetScreenDimension(context);

    return Scaffold(key: WelcomeScreenScaffoldKey, body: _body());
  }

  Widget _body() => Container(
        child: Center(child: _authOptions()),
      );

  Widget _authOptions() => Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              Expanded(
                flex: 2,
                child: ContainerClipper(
                  cliptype: ClipType(Clip.bottom_oval_bottom),
                  container: Container(
                    child: Image.asset('asset/images/login.jpg'),
                    // decoration: BoxDecoration(
                    //     gradient: LinearGradient(
                    //         begin: Alignment.topCenter,
                    //         end: Alignment.bottomCenter,
                    //         colors: coolBluesGradient)),
                  ),
                ),
              ),
              Expanded(
                  child: Container(
                padding: EdgeInsets.all((Store.dimension.width) / 10),
                alignment: Alignment.center,
                child: Text(
                    'Words have no wings but they can fly a thousand miles.',
                    textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline6,
                    ),
              ))
            ],
          ),
          Container(
            height: double.infinity,
            width: double.infinity,
            alignment: Alignment.bottomCenter,
            padding: EdgeInsets.only(left: 8.0, right: 8.0),
            child: ButtonTheme(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                  side: BorderSide(color: Colors.red)),
              minWidth: double.infinity,
              child: !_showLogin
                  ? Container()
                  : OutlineButton(
                      onPressed: (() {
                        showLoaderDialog(
                            context: context, message: "Please wait.");
                        gmailSignIn().then((user) => _userProfile(user));
                      }),
                child: Text("Login"),
              ),
            ),
          ),
        ],
      );

  void _userProfile(user) async {
    Store.authUser = user;
    var notificationToken = (await getNotificationToken());

    Log.d(tag: "User id", message: user.uid);

    await QueryType().userStatus(
        userId: user.uid,
        user: (UserModel user, Map<String, dynamic> error) {
          if (error != null) {
            if (error["error"] == "User data not present.") {
              // Signup
              // showSnackBar(
              // scaffoldKey: WelcomeScreenScaffoldKey, msg: "Please signup to join gossip.");
              Navigator.pushNamed(context, RouterPath(Path.LoginScreen));
            } else {
              showSnackBar(
                  scaffoldKey: WelcomeScreenScaffoldKey,
                  msg: "Please check your network and retry.");
            }
          } else {
            QueryType().userProfile(
                userId: user.uid,
                fcmToken: notificationToken,
                user: ((user, error) {
                  Navigator.pop(context);
                  if (error != null) {
                    showSnackBar(
                        scaffoldKey: WelcomeScreenScaffoldKey,
                        msg: "Please check your network and retry.");
          } else {
            Navigator.pushNamed(context, RouterPath(Path.HomeScreen));
          }
                }));
          }
        });
  }
}
