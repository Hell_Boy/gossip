import 'package:catchforms/store/store.dart';
import 'package:catchforms/utils/DateFormatter.dart';
import 'package:catchforms/utils/Logger.dart';
import 'package:catchforms/utils/QueryType.dart';
import 'package:catchforms/utils/ui/FullScreenDialog/ChatFullScreenDialog.dart';
import 'package:flutter/material.dart';

class Chat extends StatefulWidget {
  @override
  _ChatState createState() => _ChatState();
}

class _ChatState extends State<Chat> {
  final ChatScaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    _fetchChats();
  }

  @override
  void dispose() {
    super.dispose();
    Store.chats = null;
  }

  @override
  Widget build(BuildContext context) {
    // return Scaffold(body: _body(),);
    return Scaffold(key: ChatScaffoldKey, body: _body());
  }

  _body() {
    if (Store.chats != null)
      return _availableChatsLandingPage();
    else if (Store.firstTimeChat == 1) {
      _fetchChats();
      return _firstTimeChatLandingPage();
    } else {
      _fetchChats();
      return Center(child: CircularProgressIndicator());
    }
  }

  _firstTimeChatLandingPage() {
    return Container(
      color: Colors.white,
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Image.asset(
            'asset/images/emptyChat.png',
            scale: Store.dimension.height * 0.002,
          ),
          Padding(
            padding: EdgeInsets.all(Store.dimension.width * 0.05),
            child: Text(
              "Yayy! it's time to make new friends.",
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headline6,
            ),
          ),
          Container(
              margin: EdgeInsets.only(top: Store.dimension.height * 0.10),
              child: Text(
                "Tap on search button.",
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.subtitle1,
              ))
        ],
      ),
    );
  }

  _fetchChats() {
    QueryType().chats(list: ((chats, error) {
      Log.d(tag: "Chats", message: chats);
      setState(() {
        if (chats == null) {
          // Navigator.pop(context);
          Store.firstTimeChat = 1;
        }
      });
    }));
  }

  _availableChatsLandingPage() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: <Widget>[
          Expanded(
            child: Container(
                padding: EdgeInsets.all(3.0),
                width: double.infinity,
                alignment: Alignment.bottomLeft,
                // color: Colors.blue,
                child: Text(
                  'Chats',
                  style: Theme.of(context)
                      .textTheme
                      .headline3
                      .copyWith(color: Colors.black87, letterSpacing: 1.0),
                )),
          ),
          Expanded(
              flex: 4,
              child: ListView.builder(
                  itemCount: Store.chats.length,
                  itemBuilder: (_, index) => Card(
                        child: ListTile(
                        title: Text(
                          Store.chats.elementAt(index).chosen_name,
                            style: Theme.of(context).textTheme.headline4.copyWith(color:Colors.black87),
                          ),
                          trailing: Column(
                            children: <Widget>[
                              Text(ddMMyyyy_DateFormat(
                                  value: dateFromTimestamp(
                                      timestamp: Store.chats
                                          .elementAt(index)
                                          .timestamp))),
                              InkWell(
                                onTap: (() {
                                  Log.d(tag: "Interests", message: "topics");
                                }),
                                child: Padding(
                                  padding: EdgeInsets.only(
                                      top: Store.dimension.height * 0.02),
                                  child: Icon(Icons.whatshot, color: Colors.orange,),
                        ),
                              )
                            ],
                          ),
                          subtitle: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                                Store.chats.elementAt(index).message.content, style: Theme.of(context).textTheme.subtitle1,),
                          ),
                        onTap: (() {


                          Navigator.of(context)
                              .push(new MaterialPageRoute<Null>(
                                  builder: (BuildContext context) {
                                    return ChatFullScreenDialog(
                                      chat: Store.chats.elementAt(index));
                                  },
                                  fullscreenDialog: true));

                          // showSnackBar(
                          //     scaffoldKey: ChatScaffoldKey,
                          //     msg: Store.chats.elementAt(index).chat_id);
                        }),
                        ),
                      ))),
        ],
      ),
    );
  }
}
