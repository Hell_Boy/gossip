import 'package:catchforms/constants/ui/gradients.dart';
import 'package:catchforms/store/store.dart';
import 'package:catchforms/utils/ui/SetScreenDimension.dart';
import 'package:catchforms/utils/ui/clippers/ClipType.dart';
import 'package:catchforms/utils/ui/clippers/ContainerClipper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';


class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  final ProfileScreenScaffoldKey = GlobalKey<ScaffoldState>();
  FirebaseUser _firebaseUser;
  var nameInitials = "hh";
  @override
  void initState() {
    super.initState();
//    _firebaseUser = Store.authUser;
//    var nameKeyWords = _firebaseUser.displayName.split(' ');
//    nameKeyWords.forEach((elem) {
//      nameInitials = nameInitials + elem[0].toUpperCase();
//    });
  }
  @override
  Widget build(BuildContext context) {
    SetScreenDimension(context);
    return Scaffold(key: ProfileScreenScaffoldKey, body: _body());
  }

  Widget _body() {
    return Stack(
      children: <Widget>[_background(), _foreground()],
    );
  }

  _background() {
    return Column(
      children: <Widget>[
        Expanded(
          flex: 1,
            child: ContainerClipper(
                cliptype: ClipType(Clip.triangle_clipper),
                container: Container(
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: coolBluesGradient,
                        ))))),
        Expanded(
        flex: 2,
            child: Container()),
      ],
    );
  }

  _foreground() {
    return Column(children: <Widget>[

      Padding(padding: EdgeInsets.only(top: Store.dimension.height/15,bottom: 10) ,child:  Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CircleAvatar(
                maxRadius: Store.dimension.width / 8,
                backgroundColor: Colors.brown.shade800,
                child: Text(
                  nameInitials,
                  textScaleFactor: Store.dimension.height / 300,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                  child: Text("Eddy"),
                ),
              ),

            ],
          )),
  Expanded(flex:2,child:  _activities(),),
      ButtonTheme(
        minWidth: Store.dimension.width,
        child: RaisedButton(onPressed: (){},
          child: Text('Logout',style: TextStyle(
            color: Colors.white,
            fontSize: 14.0,
          ),),

        )
      )



    ]);
  }

 Widget _activities() => Card(
   elevation: 4.0,
   child: Column(
     children: <Widget>[
       Padding(
           padding: EdgeInsets.all(10),
         child:  Row(
           children: <Widget>[
             Expanded(child:   Text(
                 "Your Interest",
               style: TextStyle(
                 fontSize: 16,
                 fontWeight: FontWeight.bold
               ),
             )
             ),
             Icon(Icons.edit)
           ],
         ),
       ),
       Container(
         margin: EdgeInsets.all(6),
         child: chiplist(),



       ),


     ],
   ),
 );

  final ratings = Container(
    padding: EdgeInsets.all(5),
    child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Icon(Icons.star, color: Colors.green[500]),
            Icon(Icons.star, color: Colors.green[500]),
            Icon(Icons.star, color: Colors.green[500]),
            Icon(Icons.star, color: Colors.black),
            Icon(Icons.star, color: Colors.black),
          ],
        ),


  );

  chiplist(){
    return  Wrap(
      children: <Widget>[

  interestChips("IronMan", Color(0xFFff8a65)),
  interestChips("Movies", Color(0xFF4fc3f7)),
  interestChips("Games", Color(0xFF9575cd)),
  interestChips("PowerRangers", Color(0xFF4db6ac)),
  interestChips("Sports", Color(0xFF5cda64)),
  interestChips("Nature", Color(0xFFff856f)),
],);
  }

  Widget interestChips(String label,Color color){
    return Container(
        margin: EdgeInsets.all(6),
        child: Chip(
        labelPadding: EdgeInsets.all(5),
    avatar: CircleAvatar(
      backgroundColor: Colors.white,
      child: Icon(Icons.favorite,color: Colors.pinkAccent,),
    ),
      label: Text(label,style: TextStyle(
        color: Colors.white
      ),),
      backgroundColor: color,
      elevation: 6.0,
      shadowColor: Colors.grey[60],
      padding: EdgeInsets.all(5),
    ));
  }
}