
import 'package:catchforms/models/createChatModel.dart';
import 'package:catchforms/models/randomSearchUserModel.dart';
import 'package:catchforms/models/userInterestModel.dart';
import 'package:catchforms/routes/router.dart';
import 'package:catchforms/screens/Chat.dart';
import 'package:catchforms/screens/Profile.dart';
import 'package:catchforms/screens/Search.dart';
import 'package:catchforms/store/store.dart';
import 'package:catchforms/utils/firebase/FirebaseCloudMessaging.dart';
import 'package:catchforms/utils/Logger.dart';
import 'package:catchforms/utils/QueryType.dart';
import 'package:catchforms/utils/firebase/FirebaseConnectivity.dart';
import 'package:catchforms/utils/ui/Snackbar.dart';
import 'package:catchforms/utils/ui/FabBottomAppBar.dart';
import 'package:catchforms/utils/ui/dialogs/CustomDialog2.dart';
import 'package:catchforms/utils/ui/dialogs/ProgressDialog.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

enum options { aadhar, pan, voter, dl, ration }

class _HomeScreenState extends State<HomeScreen> {
  final HomeScreenKey = GlobalKey<ScaffoldState>();

  var _tabs = [Search(), Chat(), Profile()];

  int _selectedIndex = 0;

@override
  void initState() {
    super.initState();
    markAsActive();
    messageListner(onMessageReceived:((message,  error){
       showDialog(context: context,
      //  builder: (context)=>CustomDialog(title: message['notification']['title'], description: message['notification']['body'],buttonText: "Close")
       builder: (context)=>CustomDialog2(title: Text(
          message['data']['title'],
          style: TextStyle(
            fontSize: Store.dimension.height / 27,
            fontWeight: FontWeight.w700,
          ),
        ),
        description: Text(message['data']['desc']),
        circleAvatar: Text(message['data']['img'],
            style: TextStyle(
              fontSize: Store.dimension.height / 15,
            )),
        button: Text(
          'Accept',
          style: TextStyle(
            fontSize: Store.dimension.height / 47,
            fontWeight: FontWeight.w600,
          ),
        ),
        onClick: ()=>{

        },)
       );
     }));
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        key: HomeScreenKey,
        // appBar: AppBar(title: Text(TITLE_HOME_SCREEN, style: Theme
        //     .of(context)
        //     .textTheme
        //     .headline,)),
        body: _body(),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: FloatingActionButton(
          onPressed: (() {
            _searchDialog();
            _selectedTab(0);
          }),
          tooltip: 'Search',
          backgroundColor: Colors.blue[400],
          child: Icon(Icons.search),
          elevation: 5.0,
        ),
        bottomNavigationBar: Material(
          elevation: 3.0,
                  child: FABBottomAppBar(
            onTabSelected: (i) => {_selectedTab(i+1)},
            items: [
              // FABBottomAppBarItem(iconData: Icons.menu, text: 'This'),
              FABBottomAppBarItem(iconData: Icons.message, text: 'Chats'),
              FABBottomAppBarItem(iconData: Icons.person, text: 'Profile'),
              // FABBottomAppBarItem(iconData: Icons.info, text: 'Bar'),
            ],
          ),
        ));
  }

  _body() {
    return Container(
      child: _tabs[_selectedIndex],
    );
  }

  _selectedTab(int index){
    setState(() {
      _selectedIndex = index;
    });
  }

  _searchDialog() {
    showLoaderDialog(context: context, message: "Searching...");
    QueryType().randomUserSearch(response: (resp, error) {
      Navigator.pop(context);

      if (error != null) {
        Log.d(tag: "Error", message: error['error']);

        if (error['error'] == 'No match found.') {
          showSnackBar(
              scaffoldKey: HomeScreenKey,
              // duration: 5000,
              msg:
                  'No one is active with same interest, add more interest to increase chances of match.');
        } else {
        showSnackBar(
            scaffoldKey: HomeScreenKey,
            duration: 2000,
            msg: 'No one is free, please try one more time.');
        }
      } else
        _showRandomUser(resp);
      Log.d(tag: "Random Search", message: resp.toString());
    });
  }

  _showRandomUser(Map<String, dynamic> userData) {
    RandomSearchUserModel randomUserData =
        RandomSearchUserModel.fromJson(userData);

    var charArray = randomUserData.chosen_name.split('_');
    var nameKeyWord = charArray[0][0].toUpperCase();

    if (charArray.length == 1)
      nameKeyWord += charArray[0][1].toUpperCase();
    else
      nameKeyWord += charArray[charArray.length - 1][0].toUpperCase();

    Log.d(tag: "Random Selected user", message: userData.toString());

    showDialog(
      context: context,
      builder: (BuildContext context) => CustomDialog2(
        title: Text(
          randomUserData.chosen_name.replaceAll('_', ' '),
          style: TextStyle(
            fontSize: Store.dimension.height / 27,
            fontWeight: FontWeight.w700,
          ),
        ),
        description: _interests(randomUserData.interests),
        circleAvatar: Text(nameKeyWord,
            style: TextStyle(
              fontSize: Store.dimension.height / 15,
            )),
        button: Text(
          'Chat',
          style: TextStyle(
            fontSize: Store.dimension.height / 47,
            fontWeight: FontWeight.w600,
          ),
        ),
        onClick: (() {
            var ratings = {"user_1":Store.userModel.interests.toJson(), "user_2":randomUserData.interests.toJson()};

            // Log.d(tag: "Chat ratings", message: ratings.toJson());
            CreateChatModel createChat =
                CreateChatModel(Store.userModel.uid, randomUserData.uid, ratings);
                Log.d(tag: "Create chat", message: createChat.toJson());

            QueryType().createChat(
                createChat: createChat,
                chatCallback: ((chat, error) {
                setState(() {
                  _selectedIndex = 1;
                  Store.firstTimeChat = 2;
                });
                  Log.d(tag: "Create chat", message: chat.toJson());
                }));
          // showSnackBar(scaffoldKey: HomeScreenKey, msg: 'Chat created.');
        }),
      ),
    );
  }

  _interests(UserInterestModel interestModel) {
    var keys = interestModel.topics.keys.toList();
    var values = interestModel.topics.values.toList();

    final _textStyle = TextStyle(fontSize: Store.dimension.height / 60);

    return Wrap(
      children: List<Widget>.generate(
        keys.length,
        (int index) {
          return Padding(
            padding: EdgeInsets.only(
              left: Store.dimension.height / 90,
            ),
            child: Chip(
              avatar: CircleAvatar(
                // backgroundColor: Colors.grey.shade800,
                backgroundColor: Colors.blue.shade500,
                child: Text(
                  values.elementAt(index).toString(),
                  style: _textStyle,
                ),
              ),
              label: Text(
                keys.elementAt(index).toUpperCase(),
                style: _textStyle,
              ),
            ),
          );
        },
      ).toList(),
    );
  }
}
