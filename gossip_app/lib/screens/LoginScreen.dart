import 'package:catchforms/constants/jsonConstants.dart';
import 'package:catchforms/models/signupModel.dart';
import 'package:catchforms/models/userInterestModel.dart';
import 'package:catchforms/models/userLocationModel.dart';
import 'package:catchforms/routes/router.dart';
import 'package:catchforms/store/store.dart';
import 'package:catchforms/utils/Logger.dart';
import 'package:catchforms/utils/QueryType.dart';
import 'package:catchforms/utils/firebase/FirebaseCloudMessaging.dart';
import 'package:catchforms/utils/ui/FullScreenDialog/FullScreenSearchDialog.dart';
import 'package:catchforms/utils/ui/SetScreenDimension.dart';
import 'package:catchforms/utils/ui/Snackbar.dart';
import 'package:catchforms/utils/ui/clippers/ClipType.dart';
import 'package:catchforms/utils/ui/clippers/ContainerClipper.dart';
import 'package:catchforms/utils/ui/dialogs/ProgressDialog.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:geolocator/geolocator.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final LoginScreenScaffoldKey = GlobalKey<ScaffoldState>();
  final LoginScreenFormKey = GlobalKey<FormState>();
  TextEditingController _controller;
  Position position;

  FirebaseUser _firebaseUser;
  var nameInitials = ""; 
  Map<String, int> interests = {};
  bool _interestPresent = false;
  var _signUpButtonText = "Add topics";
  var _fcmToken;
  static bool _nickNameValidated = false;
  bool _completeSignUp = false;

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController();
    _firebaseUser = Store.authUser;
    getNotificationToken().then((value) {
      _fcmToken = value;
    });
    _getCurrentLocation();
    var nameKeyWords = _firebaseUser.displayName.split(' ');
     nameKeyWords.forEach((elem) {
      nameInitials = nameInitials+elem[0].toUpperCase();
    });
  }

  @override
  void dispose() {
    super.dispose();
    _controller.clear();
  }

  @override
  Widget build(BuildContext context) {
    SetScreenDimension(context);
    return Scaffold(key: LoginScreenScaffoldKey, body: _body());
  }


  Widget _body() {
    return SingleChildScrollView(
      child: Container(
        height: Store.dimension.height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: 4,
              child: ContainerClipper(
                  container: Container(
                    width: double.infinity,
                    padding: EdgeInsets.all(Store.dimension.width * 0.05),
                    color: Colors.blue[400],
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Sign Up",
                      style: Theme.of(context)
                          .textTheme
                          .headline2
                          .copyWith(color: Colors.white),
                    ),
                  ),
                  cliptype: ClipType(Clip.bottom_wave_clipper)),
            ),
            Expanded(
              flex: 3,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
                      child: Text(
                        "Welcome " + _firebaseUser.displayName + "!",
                        style: Theme.of(context)
                            .textTheme
                            .headline5
                            .copyWith(color: Colors.black87),
                      ),
                    ),
            ),
                  Form(
                      key: LoginScreenFormKey,
                      child: Center(child: _inputFields()))
                ],
          ),
            )
        ],
        ),
      ),
    );
  }

  Widget _inputFields() => Column(children: <Widget>[
        // Nic name
        Theme(
          data: new ThemeData(
            primaryColor: Colors.grey,
            primaryColorDark: Colors.grey,
          ),
          child: Container(
            margin: EdgeInsets.all(Store.dimension.width * 0.12),
            child: TextFormField(
              onChanged: ((value) {
                value = value.replaceAll(' ', '_').toLowerCase();

                if (value[0] != '@') value = '@' + value;

                _controller.value = _controller.value.copyWith(
                  text: value,
                  selection: TextSelection.fromPosition(
                    TextPosition(offset: value.length),
                  ),
                );

                _nickNameValidated = false;
              }),

              onEditingComplete: (() {
                _checkNickNameAvailability();
              }),

              textCapitalization: TextCapitalization.none,
              controller: _controller,
              // textAlign: TextAlign.center,
              decoration: InputDecoration(
                  labelText: "Nick Name",
                  // hintText: 'xavi3r',
                  border: OutlineInputBorder(
                      // gapPadding: 200.0,
                      borderRadius:
                          BorderRadius.all(new Radius.circular(20.0)))),
          validator: (value) {
            if (value.isEmpty) {
                  return 'Please enter a nick name.';
            }
            return null;
          },
        ),
          ),
        ),

        RaisedButton(
          shape: RoundedRectangleBorder(
              borderRadius:
                  BorderRadius.circular(Store.dimension.height * 0.05),
              side: BorderSide(color: Colors.blueGrey)),
          onPressed: () {
            _submit();
          },
          child: Text(
            _signUpButtonText,
            style: Theme.of(context).textTheme.subtitle1,
          ),
        )
      ]);

  _searchBar() async {
    var selectedInterests = await Navigator.of(context)
        .push(new MaterialPageRoute<Map<String, int>>(
            builder: (BuildContext context) {
              return FullScreenSearchDialog();
            },
            fullscreenDialog: true));

    if (selectedInterests.entries.toList().isNotEmpty) {
      setState(() {
        _interestPresent = true;
        _signUpButtonText = "Submit";

        selectedInterests.forEach((key, value) {
          interests[key] = 1;
        });
      });
    } else {
      showSnackBar(
          scaffoldKey: LoginScreenScaffoldKey,
          msg: "Please select a topic to continue.");
    }
    // Log.d(tag: "Interest", message: selectedInterests);
  }

  _getCurrentLocation() async {
    bool _islocationPermissionGranted =
        await Permission.location.request().isGranted;

    if (!_islocationPermissionGranted) {
      Map<Permission, PermissionStatus> statuses = await [
        Permission.location,
      ].request();

      _islocationPermissionGranted = statuses[Permission.location].isGranted;
    }

    if (_islocationPermissionGranted) {
      position = await Geolocator()
          .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    } else {
      Log.d(tag: "Search", message: "Unable to get location access");
    }
  }

  _checkNickNameAvailability() {
    showLoaderDialog(context: context, message: "Checking availability.");

    QueryType().searchNickName(
        name: _controller.text,
        result: ((isAvailable, error) {
          Navigator.pop(context);
          FocusScope.of(context).requestFocus(FocusNode());

          if (!isAvailable)
            showSnackBar(
                scaffoldKey: LoginScreenScaffoldKey,
                msg: "This nick name already taken, please try another one.");
          else{
            showSnackBar(
                scaffoldKey: LoginScreenScaffoldKey,
                msg: "Congratulations your nick name is available.");
            _nickNameValidated = true;
            _completeSignUp = true;

            if(_nickNameValidated && _completeSignUp && _interestPresent)
            _submit();
          }
          // Log.d(tag: "Nick name availability test", message: isAvailable);
        }));
  }

  _submit() {
    if (!_interestPresent)
      _searchBar();
    else {
      if (LoginScreenFormKey.currentState.validate() &&
          position != null &&
          _nickNameValidated) {
              // If the form is valid, display a snackbar. In the real world,
              // you'd often call a server or save the information in a database.

        var signUpData = SignupModel(
            _controller.text.trim(),
            Store.authUser.uid,
            Store.authUser.displayName,
            Store.authUser.email,
            _fcmToken,
            Store.authUser.photoUrl,
            UserInterestModel(interests),
            UserLocationModel(position.latitude, position.longitude));

        Log.d(tag: "Signup DATA: ", message: signUpData.toJson());

        QueryType().signUpProfile(
            body: signUpData,
            user: ((user, error) {
                if(error != null){
                  showSnackBar(scaffoldKey: LoginScreenScaffoldKey, msg: error["error"]);
                }else{
                  Navigator.pushNamed(context, RouterPath(Path.HomeScreen));
                }
              }));
      } else if (position == null) {
        Alert(
          context: context,
          type: AlertType.info,
          title: "Location",
          desc: "Please provide location to continue.",
          buttons: [
            DialogButton(
              child: Text(
                "Close",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              onPressed: (() {
                Navigator.pop(context);
                _getCurrentLocation();
              }),
              width: 120,
            )
          ],
        ).show();
            }

      else if(!_nickNameValidated){
        Log.d(tag: "Called", message: "Yest");
        _completeSignUp = true;
      _checkNickNameAvailability();
      }
      //  showSnackBar(
      //           scaffoldKey: LoginScreenScaffoldKey,
      //           msg: "This nick name already taken, please try another one.");

      else
       showSnackBar(
                scaffoldKey: LoginScreenScaffoldKey,
                msg: "Please check network connection and retry.");
    }
  }
}
