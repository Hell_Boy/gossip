import 'package:catchforms/models/mapConfig.dart';
import 'package:catchforms/store/store.dart';
import 'package:catchforms/utils/Logger.dart';
import 'package:catchforms/utils/QueryType.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:geolocator/geolocator.dart';
import 'package:permission_handler/permission_handler.dart';

// @required  : flutter_map: ^0.9.0
class Search extends StatefulWidget {
  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> {
  Position position;
  List<Marker> markers = [];
  MapOptions options;
  MapController mapController;
  MapConfig mapConfig;

  @override
  void initState() {
    super.initState();
    mapController = MapController();
    options = MapOptions(
      center: LatLng(20.5937, 78.9629),
      zoom: 4.5,
    );

    if (Store.mapConfig == null)
      QueryType().mapConfig(mapConfigCallback: (config, error) {
        setState(() {
          mapConfig = config;
        });
      });
    else {
      mapConfig = Store.mapConfig;
    }
  }

  @override
  Widget build(BuildContext context) {
    if (mapConfig == null)
      return Center(
          child: CircularProgressIndicator(backgroundColor: Colors.blue));

    if (position == null) {
      _getCurrentLocation();
    }

    return FlutterMap(
      mapController: mapController,
      options: options,
      layers: [
        new TileLayerOptions(
          urlTemplate: mapConfig.map_url,
          additionalOptions: {
            'accessToken': mapConfig.map_access_token,
            'id': mapConfig.map_id,
          },
        ),
        new MarkerLayerOptions(
          markers: markers,
        ),
      ],
    );
  }

  _getCurrentLocation() async {
    bool _islocationPermissionGranted =
        await Permission.location.request().isGranted;

    if (!_islocationPermissionGranted) {
      Map<Permission, PermissionStatus> statuses = await [
        Permission.location,
      ].request();

      _islocationPermissionGranted = statuses[Permission.location].isGranted;
    }

    if (_islocationPermissionGranted) {
      position = await Geolocator()
          .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    } else {
      Log.d(tag: "Search", message: "Unable to get location access");
    }

    setState(() {
      markers.add(new Marker(
        width: 90.0,
        height: 90.0,
        point: LatLng(position.latitude, position.longitude),
        builder: (ctx) => new Container(
          child: Icon(Icons.location_on, color: Colors.deepOrangeAccent,),
        ),
      ));

      // options = MapOptions(
      //   center: LatLng(position.latitude, position.longitude),
      //   zoom: 1.5,
      // );
    });

    mapController.move(LatLng(position.latitude, position.longitude), 4.5);
  }
}
