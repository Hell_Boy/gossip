
// region String Constants
const TAG_FILE_TO_JSON = 'File To Json';
const TAG_TOKEN = 'Token';
const TAG_ROUTER = 'Router';
const TAG_FIREBASE_REQUEST = 'Firebase Request';
const TAG_QUERYTYPE = 'Query Type';
const TITLE_WEBSCREEN = 'Web Screen';
const TITLE_HOME_SCREEN = 'Home';


const ATTENDANCE = 'Attendance';
const PUNCH = 'Punch';
const LEAVES = 'Leaves';
const HELP = 'Help';
const COUNTRY_CODE_INDIA = '91';
const PLEASE_WAIT = 'Please wait.';



const String WEBSITE_URL = "http://www.google.com";
const String FEEDBACK_URL = "http://www.google.com";
const String LOCATION_URL = "https://www.google.com/maps/search/?api=1&query=28.5701764,77.0219015";


const String singleLineLoremIpsum = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.';
// endregion


// region Boolean
const LOGS_ENABLED = true;
// endregion