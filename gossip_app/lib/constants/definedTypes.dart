
import 'package:catchforms/models/chatModel.dart';
import 'package:catchforms/models/mapConfig.dart';
import 'package:catchforms/models/userModel.dart';
import 'package:flutter/material.dart';

typedef functionCallback = void Function();
typedef responseCallback = Function(Map<String,dynamic> response, Map<String,dynamic> error);
typedef stringCallback = void Function(String value, Map<String,dynamic> error);
typedef booleanCallback = void Function(bool value, Map<String,dynamic> error);
typedef dropdownCallback = void Function(int index, String value, Map<String,dynamic> error);
typedef responseStringCallback = Function(String res, Map<String,dynamic> error);
typedef intCallback = void Function(int count, Map<String,dynamic> error);
typedef listCallback = void Function(List<dynamic> value, Map<String,dynamic> error);
typedef widgetCallback = Widget Function(GlobalKey<ScaffoldState> scaffold, Map<String,dynamic> error);
typedef userModelCallback = Function(UserModel user, Map<String,dynamic> error);
typedef mapConfigModelCallback = Function(MapConfig mapConfig, Map<String,dynamic> error);
typedef chatModelCallback = Function(ChatModel chat, Map<String,dynamic> error);
