import 'package:flutter/material.dart';


final safeBalanceGradient = [const Color(0xFF9be15d), const Color(0xFF00e3ae)];
final unConcernBalanceGradient = [const Color(0xFFabecd6), const Color(0xFFfbed96)];
final unSafeBalanceGradient = [const Color(0xFFf09819), const Color(0xFFff5858)];
final coolBluesGradient = [const Color(0xFF2193b0), const Color(0xFF6dd5ed)];
