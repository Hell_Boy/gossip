
import 'package:catchforms/constants/constant.dart';
import 'package:catchforms/screens/InfoScreen.dart';
import 'package:catchforms/screens/LoginScreen.dart';
import 'package:catchforms/screens/Profile.dart';
import 'package:catchforms/screens/WelcomeScreen.dart';
import 'package:catchforms/utils/Logger.dart';
import 'package:flutter/material.dart';
import '../screens/HomeScreen.dart';

enum Path{
  HomeScreen,
  InfoScreen,
  LoginScreen,
  WelcomeScreen,
  Profile,
  // AttendanceScreen,
  // LeaveScreen,
  // SignupScreen,
  ScreenNotAvailable
}

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {

    Log.d(tag: TAG_ROUTER, message: 'Route Name : ${settings.name}');

    switch (path(settings.name)) {

      case Path.HomeScreen:
        return MaterialPageRoute(builder: (_) => HomeScreen());
        break;

      case Path.InfoScreen:
        return MaterialPageRoute(builder: (_) => InfoScreen());
        break;

      case Path.LoginScreen:
        return MaterialPageRoute(builder: (_) => LoginScreen());
        break;

      case Path.WelcomeScreen:
        return MaterialPageRoute(builder: (_) => WelcomeScreen());
        break;

        case Path.Profile:
        return MaterialPageRoute(builder: (_) => Profile());
        break;



      default:
        Log.d(tag: TAG_ROUTER, message: '${settings.name} not availabel.');
        return MaterialPageRoute(
            builder: (_) => Scaffold(
              body: Center(
                child: Text('You are not allowed to Access this Screen.'),
              ),
            ));
    }
  }
}

class WebScreenScreenArguments {

  String url;

  WebScreenScreenArguments({this.url});
}


// region RoutePaths


String RouterPath(Path p){

  // if(Store.access != null && Store.userModel != null){
  //   var _allowedScreens = Store.access[Store.userModel.access].screens;
  //   if(_allowedScreens.contains(p.toString().replaceAll('Path.', ''))){
  //     return p.toString();
  //   }
  // }

  // var temp = [Path.LoginScreen, Path.SignupScreen];
  // if(temp.contains(p))
  //   return p.toString();
    return p.toString();

  // return Path.ScreenNotAvailable.toString();
}


path(String name){
  Path screen = Path.ScreenNotAvailable;

  for(int i = 0; i< Path.values.length; i++){
    var temp = Path.values.elementAt(i);
    if(temp.toString() == name){
      screen = temp;
      break;
    }
  }

  return screen;
}


// endregion
