// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'userActivityModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserActivityModel _$UserActivityModelFromJson(Map<String, dynamic> json) {
  return UserActivityModel(
    json['last_refreshed'] as int,
    json['status'] as int,
  );
}

Map<String, dynamic> _$UserActivityModelToJson(UserActivityModel instance) =>
    <String, dynamic>{
      'last_refreshed': instance.last_refreshed,
      'status': instance.status,
    };
