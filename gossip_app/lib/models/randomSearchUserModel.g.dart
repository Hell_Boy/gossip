// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'randomSearchUserModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RandomSearchUserModel _$RandomSearchUserModelFromJson(
    Map<String, dynamic> json) {
  return RandomSearchUserModel(
    json['chosen_name'] as String,
    json['uid'] as String,
    json['image_url'] as String,
    json['interests'] == null
        ? null
        : UserInterestModel.fromJson(json['interests'] as Map<String, dynamic>),
  )..location = json['location'] == null
      ? null
      : UserLocationModel.fromJson(json['location'] as Map<String, dynamic>);
}

Map<String, dynamic> _$RandomSearchUserModelToJson(
        RandomSearchUserModel instance) =>
    <String, dynamic>{
      'chosen_name': instance.chosen_name,
      'uid': instance.uid,
      'image_url': instance.image_url,
      'interests': instance.interests,
      'location': instance.location,
    };
