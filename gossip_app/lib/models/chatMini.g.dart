// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chatMini.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChatMini _$ChatMiniFromJson(Map<String, dynamic> json) {
  return ChatMini(
    json['user_1'] as String,
    json['user_2'] as String,
    json['chat_id'] as String,
    json['chosen_name'] as String,
    json['timestamp'] as int,
    json['status'] as int,
    json['message'] == null
        ? null
        : ChatMessage.fromJson(json['message'] as Map<String, dynamic>),
  )
    ..chat_path = json['chat_path'] as String
    ..chat_messages_path = json['chat_messages_path'] as String;
}

Map<String, dynamic> _$ChatMiniToJson(ChatMini instance) => <String, dynamic>{
      'user_1': instance.user_1,
      'user_2': instance.user_2,
      'chat_id': instance.chat_id,
      'chosen_name': instance.chosen_name,
      'chat_path': instance.chat_path,
      'chat_messages_path': instance.chat_messages_path,
      'timestamp': instance.timestamp,
      'status': instance.status,
      'message': instance.message,
    };
