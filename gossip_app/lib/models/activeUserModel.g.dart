// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'activeUserModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ActiveUserModel _$ActiveUserModelFromJson(Map<String, dynamic> json) {
  return ActiveUserModel(
    json['activeUsersPath'] as String,
    json['activeUserStatus'] as int,
    json['myStatusPath'] as String,
  );
}

Map<String, dynamic> _$ActiveUserModelToJson(ActiveUserModel instance) =>
    <String, dynamic>{
      'activeUsersPath': instance.activeUsersPath,
      'activeUserStatus': instance.activeUserStatus,
      'myStatusPath': instance.myStatusPath,
    };
