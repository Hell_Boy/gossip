// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'signupModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SignupModel _$SignupModelFromJson(Map<String, dynamic> json) {
  return SignupModel(
    json['chosen_name'] as String,
    json['uid'] as String,
    json['name'] as String,
    json['contact'] as String,
    json['fcm_token'] as String,
    json['image_url'] as String,
    json['interests'] == null
        ? null
        : UserInterestModel.fromJson(json['interests'] as Map<String, dynamic>),
    json['location'] == null
        ? null
        : UserLocationModel.fromJson(json['location'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$SignupModelToJson(SignupModel instance) =>
    <String, dynamic>{
      'chosen_name': instance.chosen_name,
      'uid': instance.uid,
      'name': instance.name,
      'contact': instance.contact,
      'fcm_token': instance.fcm_token,
      'image_url': instance.image_url,
      'interests': instance.interests,
      'location': instance.location,
    };
