// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chatModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChatModel _$ChatModelFromJson(Map<String, dynamic> json) {
  return ChatModel(
    json['user_1'] as String,
    json['user_2'] as String,
    json['ratings'] as Map<String, dynamic>,
  )
    ..chat_id = json['chat_id'] as String
    ..timestamp = json['timestamp'] as int
    ..status = json['status'] as int;
}

Map<String, dynamic> _$ChatModelToJson(ChatModel instance) => <String, dynamic>{
      'user_1': instance.user_1,
      'user_2': instance.user_2,
      'chat_id': instance.chat_id,
      'timestamp': instance.timestamp,
      'status': instance.status,
      'ratings': instance.ratings,
    };
