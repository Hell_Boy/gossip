// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'userModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserModel _$UserModelFromJson(Map<String, dynamic> json) {
  return UserModel(
    json['chosen_name'] as String,
    json['uid'] as String,
    json['name'] as String,
    json['contact'] as String,
    json['db_access_key'] as String,
    json['gender'] as String,
    json['image_url'] as String,
    json['dob'] as String,
    json['last_login'] as int,
    json['interests'] == null
        ? null
        : UserInterestModel.fromJson(json['interests'] as Map<String, dynamic>),
    json['activity'] == null
        ? null
        : UserActivityModel.fromJson(json['activity'] as Map<String, dynamic>),
  )..location = json['location'] == null
      ? null
      : UserLocationModel.fromJson(json['location'] as Map<String, dynamic>);
}

Map<String, dynamic> _$UserModelToJson(UserModel instance) => <String, dynamic>{
      'chosen_name': instance.chosen_name,
      'uid': instance.uid,
      'name': instance.name,
      'contact': instance.contact,
      'db_access_key': instance.db_access_key,
      'gender': instance.gender,
      'image_url': instance.image_url,
      'dob': instance.dob,
      'last_login': instance.last_login,
      'interests': instance.interests,
      'activity': instance.activity,
      'location': instance.location,
    };
