// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chatMessage.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChatMessage _$ChatMessageFromJson(Map<String, dynamic> json) {
  return ChatMessage(
    json['content'] as String,
    json['extra'] as String,
    json['from'] as String,
    json['type'] as String,
    json['timestamp'] as int,
    (json['reply'] as List)
        ?.map((e) => e == null
            ? null
            : ChatSubMessage.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$ChatMessageToJson(ChatMessage instance) =>
    <String, dynamic>{
      'content': instance.content,
      'extra': instance.extra,
      'from': instance.from,
      'type': instance.type,
      'timestamp': instance.timestamp,
      'reply': instance.reply,
    };
