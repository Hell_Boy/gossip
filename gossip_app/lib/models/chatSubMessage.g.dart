// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chatSubMessage.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChatSubMessage _$ChatSubMessageFromJson(Map<String, dynamic> json) {
  return ChatSubMessage(
    json['from'] as String,
    json['msg'] as String,
    json['timestamp'] as int,
  );
}

Map<String, dynamic> _$ChatSubMessageToJson(ChatSubMessage instance) =>
    <String, dynamic>{
      'from': instance.from,
      'msg': instance.msg,
      'timestamp': instance.timestamp,
    };
