// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'userInterestModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserInterestModel _$UserInterestModelFromJson(Map<String, dynamic> json) {
  return UserInterestModel(
    (json['topics'] as Map<String, dynamic>)?.map(
      (k, e) => MapEntry(k, e as int),
    ),
  );
}

Map<String, dynamic> _$UserInterestModelToJson(UserInterestModel instance) =>
    <String, dynamic>{
      'topics': instance.topics,
    };
