import 'package:catchforms/models/userActivityModel.dart';
import 'package:catchforms/models/userInterestModel.dart';
import 'package:catchforms/models/userLocationModel.dart';
import 'package:json_annotation/json_annotation.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'userModel.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
/// flutter pub run build_runner build
/// flutter pub run build_runner build --delete-conflicting-outputs

@JsonSerializable(nullable: true, includeIfNull: true)

class UserModel {


  UserModel(this.chosen_name, this.uid, this.name, this.contact, this.db_access_key, this.gender, this.image_url, this.dob, this.last_login, this.interests, this.activity);

  String chosen_name;
  String uid;
  String name;
  String contact;
  String db_access_key;
  String gender;
  String image_url;
  String dob;
  int last_login;
  UserInterestModel interests;
  UserActivityModel activity;
  UserLocationModel location;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory UserModel.fromJson(Map<String, dynamic> json) => _$UserModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$UserModelToJson(this);
}
