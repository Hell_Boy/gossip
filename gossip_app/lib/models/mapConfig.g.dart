// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'mapConfig.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MapConfig _$MapConfigFromJson(Map<String, dynamic> json) {
  return MapConfig(
    json['map_url'] as String,
    json['map_access_token'] as String,
    json['map_id'] as String,
  );
}

Map<String, dynamic> _$MapConfigToJson(MapConfig instance) => <String, dynamic>{
      'map_url': instance.map_url,
      'map_access_token': instance.map_access_token,
      'map_id': instance.map_id,
    };
