// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'createChatModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CreateChatModel _$CreateChatModelFromJson(Map<String, dynamic> json) {
  return CreateChatModel(
    json['user_1'] as String,
    json['user_2'] as String,
    json['ratings'],
  );
}

Map<String, dynamic> _$CreateChatModelToJson(CreateChatModel instance) =>
    <String, dynamic>{
      'user_1': instance.user_1,
      'user_2': instance.user_2,
      'ratings': instance.ratings,
    };
