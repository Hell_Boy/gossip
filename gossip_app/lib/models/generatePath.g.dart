// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'generatePath.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GeneratePath _$GeneratePathFromJson(Map<String, dynamic> json) {
  return GeneratePath(
    json['key'] as String,
    json['data'] as Map<String, dynamic>,
  );
}

Map<String, dynamic> _$GeneratePathToJson(GeneratePath instance) =>
    <String, dynamic>{
      'key': instance.key,
      'data': instance.data,
    };
