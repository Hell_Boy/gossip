import 'package:catchforms/models/activeUserModel.dart';
import 'package:catchforms/utils/QueryType.dart';
import 'package:firebase_database/firebase_database.dart';

import '../Logger.dart';

// Todo : Make an api for marking user active and inactive on connect and disconnect
markAsActive() async {
  // var temp = (await FirebaseDatabase.instance.reference().child('/users').once()).value;
  FirebaseDatabase.instance
      .reference()
      .child(".info/connected")
      .onValue
      .listen((event) {
    QueryType().markActive(
        result: (Map<String, dynamic> response, Map<String, dynamic> error) {
      ActiveUserModel _activeUserModel = ActiveUserModel.fromJson(response);
      Log.d(tag: "Active User Model", message: _activeUserModel.toJson());
       FirebaseDatabase.instance.reference().child(_activeUserModel.myStatusPath).onDisconnect().set(0);
    });
    Log.d(tag: "Connectivity Status", message: event.snapshot.value);
  });
}
