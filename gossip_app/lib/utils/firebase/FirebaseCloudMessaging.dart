import 'package:catchforms/constants/definedTypes.dart';
import 'package:catchforms/utils/Logger.dart';
import 'package:catchforms/utils/ui/dialogs/CustomDialog.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/widgets.dart';

// @required : firebase_messaging: ^6.0.16

FirebaseMessaging _fcm = FirebaseMessaging();

getNotificationToken(){

  return _fcm.getToken();
}

messageListner({
  @required responseCallback onMessageReceived 
}){
  // final FirebaseMessaging fcm = new FirebaseMessaging();

  _fcm.configure(
    onMessage: (Map<String, dynamic> message)async{
      Log.d(tag: "FCM received", message: message);
      onMessageReceived(message, null);
      // CustomDialog(title: message['notification']['title'], description: message['notification']['body'],buttonText: "Close");
    }
  );

}