
import 'package:catchforms/constants/constant.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';


// @required : flutter_webview_plugin: ^0.3.8

class WebScreen extends StatelessWidget {
  final String url;

  WebScreen({
    @required this.url
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      child: WebviewScaffold(
        url: url,
        withJavascript:true,
        appBar: new AppBar(
          title: new Text(TITLE_WEBSCREEN),
        ),
        withZoom: true,
        withLocalStorage: true,
        hidden: true,
        initialChild: Container(
          child: const Center(
            child: CircularProgressIndicator(),
          ),
        ),
      ),
    );
  }
}
