
import 'package:flutter/material.dart';

showSnackBar({
  @required GlobalKey<ScaffoldState> scaffoldKey,
  @required String msg,
  int duration = 3000}){
  scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(msg), duration: Duration(milliseconds: duration),));
}