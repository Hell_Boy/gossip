
import 'package:flutter/cupertino.dart';
import '../../store/store.dart';

class SetScreenDimension{

  BuildContext _context;

  SetScreenDimension(this._context){
    initialise();
  }

  initialise() {
    if (Store.dimension == null) {
      var size = MediaQuery
          .of(_context)
          .size;
      Store.dimension = Dimension(size.height, size.width);
    }
  }

}

class Dimension {

  double _height;
  double _width;

  double get height => _height;

  set height(double value) {
    _height = value;
  }

  Dimension(this._height, this._width);

  double get width => _width;

  set width(double value) {
    _width = value;
  }
}