import 'package:flutter/material.dart';

showLoaderDialog({@required BuildContext context, String message = "Loading...", bool dissmissble = false}){
    AlertDialog alert=AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(),
          Container(margin: EdgeInsets.only(left: 7),child:Text(message)),
        ],),
    );
    showDialog(barrierDismissible: dissmissble,
      context:context,
      builder:(BuildContext context){
        return alert;
      },
    );
  }