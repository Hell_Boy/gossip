import 'package:catchforms/constants/definedTypes.dart';
import 'package:catchforms/store/store.dart';
import 'package:catchforms/utils/ui/SetScreenDimension.dart';
import 'package:flutter/material.dart';

class CustomDialog2 extends StatelessWidget {
  final Widget title, description, button, circleAvatar;
  final functionCallback onClick;

  CustomDialog2({
    @required this.title,
    @required this.description,
    @required this.button,
    @required this.circleAvatar,
    @required this.onClick
  });

  var padding = 0.0;
  var avatarRadius = 0.0;

  @override
  Widget build(BuildContext context) {
    SetScreenDimension(context);
    padding = Store.dimension.width/40;
    avatarRadius = Store.dimension.width/7;

    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(padding),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context),
    );
  }

  dialogContent(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(
            top: avatarRadius + padding,
            bottom: padding,
            left: padding,
            right: padding,
          ),
          margin: EdgeInsets.only(top: avatarRadius),
          decoration: new BoxDecoration(
            color: Colors.white,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(padding),
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 10.0,
                offset: const Offset(0.0, 10.0),
              ),
            ],
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min, // To make the card compact
            children: <Widget>[
              title,
              SizedBox(height: padding),
              description,
              SizedBox(height: 24.0),
              Align(
                alignment: Alignment.bottomRight,
                child: FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                    onClick(); // To close the dialog
                  },
                  child: button,
                ),
              ),
            ],
          ),
        ),
        Positioned(
          left: padding,
          right: padding,
          child: CircleAvatar(
            backgroundColor: Colors.blueAccent,
            radius: avatarRadius,
            child: circleAvatar,
          ),
        ),
      ],
    );
  }
}
