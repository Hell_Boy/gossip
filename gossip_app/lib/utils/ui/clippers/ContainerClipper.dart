
import 'package:flutter/material.dart';



class ContainerClipper extends StatelessWidget {

  Widget container;
  dynamic cliptype;
  ContainerClipper({
    @required this.container,
    @required this.cliptype,
  });

  @override
  Widget build(BuildContext context) {
    return ClipPath(
      child: container,
      clipper: cliptype,
    );
  }
}
