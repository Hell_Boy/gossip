
import 'package:flutter/material.dart';

class ImageClipper extends StatelessWidget {

  Image image;
  dynamic cliptype;
  ImageClipper({
    @required this.image,
    @required this.cliptype});

  @override
  Widget build(BuildContext context) {
    return ClipPath(
      child: image,
      clipper: cliptype,
    );
  }
}
