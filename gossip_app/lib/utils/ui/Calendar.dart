
// import 'package:catchforms/constants/definedTypes.dart';
// import 'package:flutter/material.dart';
// import 'package:table_calendar/table_calendar.dart';

// // @required table_calendar: ^2.2.1

// import 'package:flutter/material.dart';

// class Calendar extends StatefulWidget {

//   stringCallback onDaySelectedLong;
//   Calendar({Key key, @required this.onDaySelectedLong}) : super(key:key);

//   @override
//   _CalendarState createState() => _CalendarState();
// }

// class _CalendarState extends State<Calendar> {

//   CalendarController _calendarController;


//   @override
//   void initState() {
//     super.initState();
//     _calendarController = CalendarController();
//   }

//   @override
//   void dispose() {
//     _calendarController.dispose();
//     super.dispose();
//   }


//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         backgroundColor: Theme.of(context).primaryColor,
//         body : _body()
//     );
//   }

//   Widget _body(){
//     return Container(
//       child: TableCalendar(
//           calendarController: _calendarController,
//         onDayLongPressed: ((dateTime, dynamicList){
//           widget.onDaySelectedLong(dateTime.millisecondsSinceEpoch.toString());
//         }),
//       ),
//     );
//   }
// }
