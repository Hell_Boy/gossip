

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget Progress({
  String text,
  Color textColor
}){

  return Center(
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        CircularProgressIndicator(),
        Padding(
          padding: const EdgeInsets.only(top: 20),
          child: Text(text, style: TextStyle(color: textColor ),),
        )
      ],
    ),

  );
}