import 'dart:async';

import 'package:catchforms/store/store.dart';
import 'package:catchforms/utils/QueryType.dart';
import 'package:catchforms/utils/ui/dialogs/ProgressDialog.dart';
import 'package:flutter/material.dart';

import '../../Logger.dart';

// ignore: must_be_immutable
class FullScreenSearchDialog extends StatefulWidget {

  @override
  _FullScreenSearchDialogState createState() => _FullScreenSearchDialogState();
}

class _FullScreenSearchDialogState extends State<FullScreenSearchDialog> {
  TextEditingController _editingController;
  Map<String, int> _searchResults = {};
  Map<String, int> _addedSearchResults = {};
  bool _saveInterest = false;

  @override
  void initState() {
    super.initState();
    _editingController = new TextEditingController();
  }

  @override
  void dispose() {
    super.dispose();
    _editingController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(resizeToAvoidBottomInset: false, body: _body());
  }

  Widget _body() {
    return Column(
      children: <Widget>[
        Expanded(
            flex: 2,
            child: Container(
                margin: EdgeInsets.only(
                  top: Store.dimension.height * 0.02,
                  left: Store.dimension.height * 0.02,
                  right: Store.dimension.height * 0.02,
                ),
                alignment: Alignment.bottomCenter,
                child: Theme(
                  data: new ThemeData(
                    primaryColor: Colors.black54,
                    primaryColorDark: Colors.black54,
                  ),
                  child: TextFormField(
                    controller: _editingController,
                    autofocus: !_saveInterest,
                    onChanged: ((text) {
                      // Log.d(tag: "Search", message: text);

                      text = text.replaceAll(' ', '_').toLowerCase();
                _editingController.value = _editingController.value.copyWith(
                  text: text,
                  selection: TextSelection.fromPosition(
                    TextPosition(offset: text.length),
                  ),
                );
                    }),
                    onEditingComplete: (() {
                      _search();
                    }),
                    decoration: InputDecoration(
                      fillColor: Colors.blueGrey,
                      focusColor: Colors.grey,
                      contentPadding:
                          EdgeInsets.all(Store.dimension.height * 0.02),
                      suffixIcon: IconButton(
                          icon: Icon(Icons.search),
                          onPressed: (() {
                            _search();
                            _editingController.clear();
                          })),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(
                            Store.dimension.height * 0.02),
                      ),
                      labelText: "Search",
                      hintText: "Interest",
                      hintStyle: TextStyle(color: Colors.grey[400]),
                    ),
                  ),
                ))),
        Expanded(
            flex: 6,
            child: Container(
              margin: EdgeInsets.all(Store.dimension.height * 0.03),
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: _searchResults.keys.toList().length,
                  itemBuilder: ((_, index) {
                    var key = _searchResults.keys.toList().elementAt(index);
                    var interestWord =
                        key + ' ( ' + _searchResults[key].toString() + ' )';
                    return Container(
                      padding: EdgeInsets.only(
                          left: Store.dimension.height * 0.02,
                          top: Store.dimension.height * 0.02),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              child: Text(
                            interestWord,
                            style: TextStyle(
                                fontSize: Store.dimension.height * 0.023),
                          )),
                          IconButton(
                              icon: Icon(
                                Icons.add_circle_outline,
                                color: Colors.black54,
                              ),
                              onPressed: (() {
                                // Log.d(tag: "Added", message: "Called");
                                setState(() {
                                  _saveInterest = true;
                                  FocusScope.of(context).unfocus();
                                  _editingController.clear();
                                  _addedSearchResults[key] =
                                      _searchResults[key];
                                });
                              }))
                        ],
                      ),
                    );
                  })),
            )),
        Expanded(
          flex: 3,
          child: SingleChildScrollView(
            child: Wrap(
              children: _addedSearchResults.entries
                  .map((e) => Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: InkWell(
                          onTap: (() {
                            setState(() {
                              _addedSearchResults.remove(e.key);
                            });
                          }),
                          child: Chip(
                            avatar: CircleAvatar(
                              backgroundColor: Colors.grey.shade800,
                              child: Icon(
                                Icons.close,
                                size: Store.dimension.height * 0.025,
                              ),
                            ),
                            label: Text(e.key),
                          ),
                        ),
                      ))
                  .toList(),
            ),
          ),
        ),
        Expanded(
            child: !_saveInterest
                ? Container()
                : Padding(
                    padding:
                        EdgeInsets.only(bottom: Store.dimension.height * 0.02),
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(Store.dimension.height*0.8),
                          side: BorderSide(color: Colors.blueGrey)),
                      onPressed: (() {
                        // showLoaderDialog(context: context, message: "Saved");
                        // Store.selectedInterests = _addedSearchResults;
                        Navigator.of(context).pop(_addedSearchResults);
                      }),
                      child: Text(
                        "Save",
                        style:
                            TextStyle(fontSize: Store.dimension.height * 0.025),
                      ),
                    ),
                  ))
      ],
    );
  }

  _search() {
    showLoaderDialog(context: context, message: "Searching...");

    QueryType().searchSignup(
        keyword: _editingController.text,
        result: ((value, error) {
          Log.d(tag: "Search Result", message: value);
          Navigator.pop(context);

          setState(() {
            _searchResults = value.cast<String,int>();
          });
        }));
  }
}
