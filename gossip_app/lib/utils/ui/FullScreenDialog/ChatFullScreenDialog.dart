import 'dart:async';

import 'package:catchforms/models/chatMessage.dart';
import 'package:catchforms/models/chatMini.dart';
import 'package:catchforms/store/store.dart';
import 'package:catchforms/utils/firebase/FirebaseConnectivity.dart';
import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';

import '../../Logger.dart';

// ignore: must_be_immutable
class ChatFullScreenDialog extends StatefulWidget {
  ChatMini chat;

  ChatFullScreenDialog({
    Key key,
    @required this.chat,
  }) : super(key: key);

  @override
  _ChatFullScreenDialogState createState() => _ChatFullScreenDialogState();
}

class _ChatFullScreenDialogState extends State<ChatFullScreenDialog> {
  var chatScaffoldKey = GlobalKey<ScaffoldState>();
  ScrollController _scrollController = new ScrollController();
  List<Widget> messages = [];
  TextEditingController _textEditingController;
  FocusNode inputFieldNode;

  bool firstTimeChatLoad = true;


  var sortedKeys = [];

  @override
  void initState() {
    super.initState();
    markAsActive();
    _scrollController = new ScrollController();
//    _scrollToLast();

    _textEditingController = new TextEditingController();
    inputFieldNode = FocusNode();

// Old Messages
    FirebaseDatabase.instance
        .reference()
        .child(widget.chat.chat_messages_path)
         .limitToLast(30)
        .once()
        .then(((snapshot) {
      if (snapshot.value != null) {
        // Log.d(tag: "Old messages", message: snapshot.value);

        var tempSortedKeys = snapshot.value.keys.toList()..sort();
        sortedKeys = tempSortedKeys.reversed.toList();

//        Log.d(tag: "keys", message: sortedKeys);

        for (var key in sortedKeys) {
          var messageContent = snapshot.value[key];
          messageContent['message_id'] = key;
          messages.add(_previousMessages(messageContent));
        }

        setState(() {});
      }
    }));
  }

  @override
  Widget build(BuildContext context) {
    Log.d(tag: "Message Obj", message: widget.chat.chat_messages_path);
    return Scaffold(
        key: chatScaffoldKey,
        backgroundColor: Theme.of(context).primaryColor,
        appBar: AppBar(
          backgroundColor: const Color(0xFFF5F6F1),
          title: Text(widget.chat.chosen_name,
              style: Theme.of(context).textTheme.headline),
          leading: IconButton(
              icon: Icon(Icons.keyboard_return),
              onPressed: (() {
                Navigator.pop(context);
              })),
        ),
        body: Container(
          color: const Color(0xFFF5F6F1),
          child: StreamBuilder<Event>(
              stream: FirebaseDatabase.instance
                  .reference()
                  .child(widget.chat.chat_messages_path)
                  // .limitToLast(40)
                  .onChildAdded,
              builder: (BuildContext context, AsyncSnapshot<Event> event) {
                if (event.hasData && event.data.snapshot.value != null) {
                  // Log.d(tag: "Message ", message: event.data.snapshot.value);
                  Map messagesObj = event.data.snapshot.value;
                  String messagesKey = event.data.snapshot.key;

                   if(!sortedKeys.contains(messagesKey)){
                     sortedKeys.add(messagesKey);
//                     Log.d(tag: "Message Ids", message: sortedKeys);
                  messagesObj['message_id'] = messagesKey;
//                     messages.add(_previousMessages(messagesObj));
                   messages.insert(0, _previousMessages(messagesObj));
                   }

                }
                return _body(messages);
              }),
        ));
  }

  // Widget _body(List<Widget> messages) {
  //   return Align(
  //     alignment: Alignment.bottomCenter,
  //     child: Padding(
  //       padding: EdgeInsets.all(Store.dimension.width / 60),
  //       child: ListView.builder(
  //           controller: _scrollController,
  //           shrinkWrap: true,
  //           itemCount: messages.length + 1,
  //           itemBuilder: (_, index) => index < messages.length
  //               ? messages.elementAt(index)
  //               : _createMessage()),
  //     ),
  //   );
  // }

  Widget _body(List<Widget> messages) {
    return SingleChildScrollView(
        child: Column(
      children: <Widget>[
        Container(
            height: Store.dimension.height * 0.75,
            alignment: Alignment.bottomCenter,
            padding: EdgeInsets.all(Store.dimension.width / 60),
            child: ListView.builder(
                controller: _scrollController,
                shrinkWrap: true,
                itemCount: messages.length,
                reverse: true,
                itemBuilder: (_, index) => messages.elementAt(index))
            // index < messages.length
            //     ? messages.elementAt(index)
            //     : _createMessage()),
            ),
        Container(
          height: Store.dimension.height * 0.10,
          // margin: EdgeInsets.only(top: Store.dimension.height*0.01),
          child: _createMessage(),
        )
      ],
    ));
  }

  _previousMessages(value) {
    ChatMessage msg = ChatMessage.fromJson((value).cast<String, dynamic>());

    bool _sameUser = msg.from == Store.userModel.uid;

    return Row(
      mainAxisAlignment:
          _sameUser ? MainAxisAlignment.end : MainAxisAlignment.start,
      children: <Widget>[
        Container(
          width: Store.dimension.width*0.70,
            margin: EdgeInsets.all(Store.dimension.height*0.01),
            padding: const EdgeInsets.all(3.0),
            decoration:
                BoxDecoration(border: Border.all(color: _sameUser ? Colors.blueAccent : Colors.grey),  borderRadius: BorderRadius.all(Radius.circular(20))),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(msg.content.toString()),
            )),
      
      ],
    );
  }

  // bool imageSelected = false;

  _createMessage() {
    return Card(
      child: Row(
        children: <Widget>[
          Expanded(
              flex: 1,
              child: IconButton(
                icon: Icon(Icons.attach_file),
              )),
          Expanded(
              flex: 6,
              child: TextFormField(
                autofocus: true,
                controller: _textEditingController,
                keyboardType: TextInputType.multiline,
                maxLines: null,
                focusNode: inputFieldNode,
                onFieldSubmitted: (String) => inputFieldNode.requestFocus(),
              )),
          Expanded(
              flex: 1,
              child: IconButton(
                icon: Icon(Icons.keyboard_arrow_up),
                onPressed: (() {
                  // Log.d(tag: "Chat Message", message: _textEditingController);
                  //  FocusScope.of(context).requestFocus(inputFieldNode);4
//                  _scrollToLast();
                  inputFieldNode.requestFocus();
                  ChatMessage msg = ChatMessage(_textEditingController.text, "",
                      Store.userModel.uid, "string", -1, null);
                  _sendMessage(msg);
                  _textEditingController.clear();
                }),
              ))
        ],
      ),
    );
  }

  _sendMessage(ChatMessage msg) {
    Log.d(tag: "Message Json", message: msg.toJson());
    var msgJson = msg.toJson();
    
    msgJson['timestamp'] = ServerValue.timestamp;
    var messageId = FirebaseDatabase.instance
        .reference()
        .child(widget.chat.chat_messages_path)
        .push()
        .key;
    FirebaseDatabase.instance
        .reference()
        .child(widget.chat.chat_messages_path + '/' + messageId)
        .set(msgJson);
  }

  _scrollToLast() {
    if (firstTimeChatLoad) {
      firstTimeChatLoad = false;
      Timer(
          Duration(seconds: 1),
          () => _scrollController
              .jumpTo(_scrollController.position.maxScrollExtent));
    } else {
      Timer(
          Duration(milliseconds: 800),
          () => _scrollController
              .jumpTo(_scrollController.position.maxScrollExtent));
      // _scrollController.jumpTo(_scrollController.position.maxScrollExtent);
    }
  }

  @override
  void dispose() {
    print("dispose was called");
    _textEditingController = new TextEditingController();
    inputFieldNode.dispose();
    super.dispose();
  }
}
