import 'dart:convert';

import 'package:catchforms/store/store.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;

import 'Logger.dart';

// @Required :  http: ^0.12.1

class Http {
  static const _domain =
      "https://us-central1-gossip-6d205.cloudfunctions.net/gossipApi";

  postRequest(
      {@required endpoint, dynamic body, bool isSignUp = false, bool isLogin}) async {
    final url = _domain + endpoint;

    Log.d(tag: "Url", message: url);

    var response;
    if (body != null && isSignUp) {
      Log.d(
          tag: "Header",
          message: {
            "signup": "true",
            'Content-type': 'application/json',
          }.toString());
      response = await http.post(url,
          headers: {
            "signup": "true",
            'Content-type': 'application/json',
          },
          body: jsonEncode(body));
    } else if (body != null) {
      Log.d(
          tag: "Header",
          message: {
            "access_token": Store.userModel.db_access_key,
            "user_id": Store.userModel.uid
          }.toString());

      Log.d(
          tag: "Body",
          message: body.toString());

      response = await http.post(url,
          headers: {
            "access_token": Store.userModel.db_access_key,
            "user_id": Store.userModel.uid
          },
          body: body);
    } else if(isLogin == true) {
// Only for login
      Log.d(
          tag: "Header",
          message: {"login": "true", "user_id": Store.authUser.uid}.toString());

      response = await http.post(url, headers: {"login": "true", "user_id": Store.authUser.uid});
    }else{
      Log.d(
          tag: "Header",
          message: {
            "access_token": Store.userModel.db_access_key,
            "user_id": Store.userModel.uid
          }.toString());

      response = await http.post(url,
          headers: {
            "access_token": Store.userModel.db_access_key,
            "user_id": Store.userModel.uid
          },);
    }

    // Log.d(tag: "Header", message: headers.toString());

    // Map<String, dynamic> map =
    //     new Map<String, dynamic>.from(jsonDecode(response.body).cast<String, dynamic>());

    Log.d(tag: "Response", message: response.body.toString());

    return jsonDecode(response.body);
  }
}
