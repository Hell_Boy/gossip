

import 'dart:convert';

import 'package:catchforms/constants/definedTypes.dart';
import 'package:catchforms/models/chatMini.dart';
import 'package:catchforms/models/chatModel.dart';
import 'package:catchforms/models/createChatModel.dart';
import 'package:catchforms/models/mapConfig.dart';
import 'package:catchforms/models/signupModel.dart';
import 'package:catchforms/models/userModel.dart';
import 'package:catchforms/store/store.dart';
import 'package:catchforms/utils/Http.dart';
import 'package:catchforms/utils/Logger.dart';
import 'package:flutter/material.dart';

class QueryType{

  mapConfig({
    @required mapConfigModelCallback mapConfigCallback,
  })async {

    var responseData = await Http().postRequest(endpoint: "/master/map");
    
    Log.d(tag: "MapConfig", message: responseData);

    if(responseData["error"] !=null){

      mapConfigCallback(null, responseData);

    }else{
      Store.mapConfig = MapConfig.fromJson(responseData);
      mapConfigCallback(Store.mapConfig, null);
    }
  }

createChat({
  @required CreateChatModel createChat,
    @required chatModelCallback chatCallback,
  })async {

    Map<String,dynamic> _body = createChat.toJson();
    // Map<String,dynamic> _body = {"user_1": "JRWUqgT9Pqa3UvVVPpnwvoMzzFJ3", "user_2": "user_2", "ratings": {"user_1": {"topics": {"aaa": 1, "bb": 1}}, "user_2": {"topics": {"aaa": 1, "bb": 1}}}};

    var responseData = await Http().postRequest(endpoint: "/chat/create", body: jsonEncode(_body));

    Log.d(tag: "Create Chat", message: responseData);

    if(responseData["error"] !=null){

      chatCallback(null, responseData);

    }else{
      ChatModel chat = ChatModel.fromJson(responseData);
      chatCallback(chat, null);
    }
  }

chats({
    @required listCallback list,
  })async {


    var responseData = await Http().postRequest(endpoint: "/chat/list");

    Log.d(tag: "Create Chat", message: responseData);

    if(responseData["error"] !=null){

      list(null, responseData);

    }else{
      List<ChatMini> _chats = [];

      for(var data in responseData['chats']){
        _chats.add(ChatMini.fromJson(data));
      }

      Store.chats = _chats;

      list(Store.chats, null);
    }
  }



randomUserSearch({
    @required responseCallback response,
  })async {

    var responseData = await Http().postRequest(endpoint: "/user/randomMatch");

    Log.d(tag: "RandomUserSearch", message: responseData);

    if(responseData["error"] !=null){

      response(null, responseData);

    }else{
      response(responseData, null);
    }
  }



 userStatus({
    @required String userId,
    @required userModelCallback user,
  })async {

    var responseData = await Http().postRequest(endpoint: "/user/checkStatus", isLogin: true);
    
    Log.d(tag: "Profile", message: responseData);

    if(responseData["error"] !=null){

      user(null, responseData);

    }else{
      Store.userModel = UserModel.fromJson(responseData);
      user(Store.userModel, null);
    }
  }

 userProfile({
    @required String userId,
    @required String fcmToken,
    @required userModelCallback user,
  })async {
    Map<String, dynamic> tempRequestBody = {
      "user_id": userId,
      "fcm_token": fcmToken
    };

    var responseData = await Http().postRequest(endpoint: "/user/profile",body: tempRequestBody);
    
    Log.d(tag: "Profile", message: responseData);

    if(responseData["error"] !=null){

      user(null, responseData);

    }else{
      Store.userModel = UserModel.fromJson(responseData);
      user(Store.userModel, null);
    }
  }


  signUpProfile({
    @required SignupModel body,
    @required userModelCallback user,
  })async {
    var responseData = await Http()
        .postRequest(endpoint: "/user/signup", body: body.toJson(), isSignUp: true);
    
    Log.d(tag: "Profile", message: responseData);

    if(responseData["error"] !=null){

      user(null, responseData);

    }else{
      Store.userModel = UserModel.fromJson(responseData);
      user(Store.userModel, null);
    }
  }

  searchSignup({
    @required String keyword,
    @required responseCallback result,
  }) async {
    var responseData = await Http().postRequest(
        endpoint: "/search/topics", body: {"keyword": keyword}, isSignUp: true);

    Log.d(tag: "Search", message: responseData);

    if (responseData["error"] != null) {
      result(null, responseData);
    } else {
      result(responseData, null);
    }
  }


  searchNickName({
    @required String name,
    @required booleanCallback result,
  }) async {
    var responseData = await Http().postRequest(
        endpoint: "/user/checkNickNameAvailability", body: {"nick_name": name}, isSignUp: true);

    Log.d(tag: "Nick Name Search", message: responseData);

    if (responseData["error"] != null) {
      result(null, responseData);
    } else {
      result(responseData["status"], null);
    }
  }


  markActive({
    @required responseCallback result,
  }) async {
    var responseData = await Http().postRequest(
        endpoint: "/user/markActive");

    Log.d(tag: "Mark Active", message: responseData);

    if (responseData["error"] != null) {
      result(null, responseData);
    } else {
      result(responseData, null);
    }
  }



}



// class FirebaseRequest{


//   static getData({
//     @required String path,
//     @required responseCallback onResponse
//   }){

//     databaseReference.child(path).once().then((data){

//       if(data.value != null)
//         onResponse(data.value.cast<String,dynamic>());
//       else
//         onResponse(null);

//     }).catchError((e){
//       onResponse(null);
//       Log.d(tag: TAG_FIREBASE_REQUEST, message: 'Get Exception : $e');
//     });
//   }


//   static put({
//     @required String path,
//     @required Map<String,dynamic> data,
//     @required booleanCallback onResponse
//   }){

//     bool completed = false;

//     databaseReference.child(path).set(data).whenComplete((){
//       completed = true;
//       onResponse(completed);
//     }).catchError((e){
//       onResponse(completed);
//       Log.d(tag: TAG_FIREBASE_REQUEST, message: 'Put Exception : $e');
//     });
//   }



//   static generateKey({
//     @required String path,
//     @required stringCallback key
//   }){
//     key(databaseReference.child(path).push().key);
//   }
// }