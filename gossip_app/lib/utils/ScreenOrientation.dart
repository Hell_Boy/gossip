
import 'package:flutter/services.dart';

class ScreenOrientation{

  setPotraitMode(){
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }
}