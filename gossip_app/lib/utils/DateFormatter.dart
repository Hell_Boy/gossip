
// Required : From flutter pub intl: ^0.16.0

import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';

import 'Logger.dart';

const TAG_DATEFORMATTER = 'Date time formatter';

String ddMMyyyy_DateFormat({
  @required dynamic value}){

  try {
    var temp = value is String ? DateTime.parse(value) : value;
    var formatter = DateFormat('dd-MM-yyyy');
    return formatter.format(temp);
  }
  catch(e){
    return null;
  }
}

String yyyyMMdd_DateFormat({
  @required dynamic value}){

  try {
    var temp = value is String ? DateTime.parse(value) : value;
    var formatter = DateFormat('yyyy-MM-dd');
    return formatter.format(temp);
  }
  catch(e){
    return null;
  }
}

String hhmma_TimeFormat({
  @required dynamic value}){

  try {
    var temp = value is String ? DateTime.parse(value) : value;
    var formatter = DateFormat('h:mm a');
    return formatter.format(temp);
//    return DateFormat.Hm().format(temp).toString();
  }
  catch(e){
    return null;
  }
}



String timeStampSinceEpoch({@required String value}){

  try{
    var date = DateTime.parse(value);
    Log.d(tag: TAG_DATEFORMATTER, message: 'Epoch : ${date.toString()}');
    return date.millisecondsSinceEpoch.toString();
  }catch(e){
    Log.d(tag: TAG_DATEFORMATTER, message: 'Exception : $e');
    return null;
  }
}

String dateFromTimestamp({@required int timestamp}){
  try{
    var date = DateTime.fromMillisecondsSinceEpoch(timestamp*1000).toLocal();
    Log.d(tag: TAG_DATEFORMATTER, message: 'Epoch : ${date.toString()}');
    return date.toString();
  }catch(e){
    Log.d(tag: TAG_DATEFORMATTER, message: 'Exception : $e');
    return null;
  }
}