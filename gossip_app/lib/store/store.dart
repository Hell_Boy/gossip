import 'package:catchforms/models/chatMini.dart';
import 'package:catchforms/models/mapConfig.dart';
import 'package:catchforms/models/userModel.dart';
import 'package:firebase_auth/firebase_auth.dart';

import '../utils/ui/SetScreenDimension.dart';

// @required   firebase_auth: ^0.14.0+5
// @required  firebase_database: ^3.0.7

class Store{
 static FirebaseUser _authUser;

 static FirebaseUser get authUser => _authUser;

 static set authUser(FirebaseUser value) {
   _authUser = value;
 }

  static List<ChatMini> _chats = null;

  static List<ChatMini> get chats => _chats;
  static set chats(value) => _chats = value;

static UserModel _userModel;

static UserModel get userModel => _userModel;
static set userModel(value) => _userModel = value;

static MapConfig _mapConfig;

static MapConfig get mapConfig => _mapConfig;
static set mapConfig(value) => _mapConfig = value;

 static Dimension _dimension;

 static Dimension get dimension => _dimension;

 static set dimension(Dimension value) {
   _dimension = value;
 }

  static int _firstTimeChat = 0;
  static int get firstTimeChat => _firstTimeChat;
  static set firstTimeChat(value)=> _firstTimeChat = value;

  static bool _nickNameValidated = false;
  static bool get nickNameValidated => _nickNameValidated;
  static set nickNameValidated (value)=> _nickNameValidated = value;
}
